import pandas as pd

# read csv file
df = pd.read_csv('./HRprops120.csv')

# perform groupby
res = df.groupby(['SessionID','EmpaticaID'])[['fft_pLF' ,'fft_pHF' ,'fft_LFHFratio' ,'fft_VLF' ,'fft_LF' ,'fft_HF' ,'lomb_lf' ,'RMSSD' , 'PNN50' ,'TRI' ,'TINN' ,'lomb_hf', 'meanV' ,'stdV' ,'sdsd' ,'qr' ,'SessionLabel']].mean().reset_index()
print(res)



# write to csv
res.to_csv('average_sessions.csv', index=False)