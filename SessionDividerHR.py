#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 25 22:00:27 2018

@author: root
"""
import pandas as pd
import numpy as np
import csv
import string


try:
    features = pd.read_csv('./HRprops120.csv')
    eventLogs = pd.read_csv('./inzva_timestamps.csv')
except:
    print("File not found exception")


    # parsing files into python arrays
fft_pLF = features['fft_pLF']
fft_pHF = features['fft_pHF']
fft_LFHFratio = features['fft_LFHFratio']
fft_VLF = features['fft_VLF']
fft_LF = features['fft_LF']
fft_HF = features['fft_HF']
lomb_lf = features['lomb_lf']
RMSSD = features['RMSSD']
PNN50 = features['PNN50']
TRI = features['TRI']
TINN = features['TINN']
lomb_hf = features['lomb_hf']
meanV = features['meanV']
stdV = features['stdV']
startLabels = features['startTime']
endLabels = features['endTime']
sdsd = features['sdsd']
qr = features['qr']
EmpaticaID = features['EmpaticaID']
sessionNumber = eventLogs.ix[:, 0]
sessionStart = eventLogs.ix[:, 1]
sessionEnd = eventLogs.ix[:, 2]
sessionLabel = eventLogs.ix[:, 3]
#sessionLabel2 = eventLogs.ix[:, 5]
#sessionLabel3 = eventLogs.ix[:, 6]
k=sessionStart[1]
l=startLabels[1]
sessionMat=np.zeros((len(startLabels),18),dtype=float)
sessionMat.fill(np.nan)
session_string= np.empty(len(startLabels),dtype='S10')


session_label_string= np.empty(len(startLabels),dtype='S10')


empatica_string= np.empty(len(startLabels),dtype='S10')





for i in range(1, len(startLabels)):
    for j in range(0,len(eventLogs)):
        if(sessionStart[j] <= float(startLabels[i]) <= sessionEnd[j]):
            sessionMat[i,0]=fft_pLF.iloc[i]
            sessionMat[i, 1] = fft_pHF.iloc[i]
            sessionMat[i, 2] = fft_LFHFratio.iloc[i]
            sessionMat[i,3] = fft_VLF.iloc[i]
            sessionMat[i,4] = fft_LF.iloc[i]
            sessionMat[i,5] = fft_HF.iloc[i]
            sessionMat[i,6] = lomb_lf.iloc[i]
            sessionMat[i,7]=RMSSD.iloc[i]
            sessionMat[i,8]=startLabels.iloc[i]
            sessionMat[i,9]=endLabels.iloc[i]
            sessionMat[i, 10] = PNN50.iloc[i]
            sessionMat[i, 11] = TRI.iloc[i]
            sessionMat[i, 12] = TINN.iloc[i]
            sessionMat[i,13] = lomb_hf.iloc[i]
            sessionMat[i, 14] = meanV.iloc[i]
            sessionMat[i, 15] = stdV.iloc[i]
            sessionMat[i, 16] = sdsd.iloc[i]
            sessionMat[i, 17] = qr.iloc[i]
            session_string[i] =sessionNumber.iloc[j]
            session_label_string[i]=sessionLabel.iloc[j]
            empatica_string[i] = EmpaticaID.iloc[i]
 #           session_label_string3[i] = sessionLabel3[j]


sessionMat=pd.DataFrame(sessionMat,columns=['fft_pLF', 'fft_pHF', 'fft_LFHFratio', 'fft_VLF', 'fft_LF', 'fft_HF', 'lomb_lf', 'RMSSD', 'startLabels', 'endLabels', 'PNN50', 'TRI','TINN','lomb_hf', 'meanV','stdV','sdsd','qr'])
session_id_string=pd.DataFrame(session_string, columns=['SessionID'])
session_label_string=pd.DataFrame(session_label_string,columns=['SessionLabel'])
session_string=pd.DataFrame(empatica_string,columns=['EmpaticaID'])

frame=pd.concat([sessionMat,session_id_string,session_label_string,session_string],axis=1)

# Convert list of tuples to dataframe and set column names and indexes

#frame.dropna(axis=0, how='any', inplace=True)

frame.to_csv('./HRson.csv', encoding='utf-8')


