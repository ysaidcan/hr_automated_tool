You can use FilterInterpolateWithPercentage and FılterWithPercentage functions.

An example call of these functions is demonstrated at BatchProcessing.m. It will extract features from all files selected in the folder.

For dividing the data into sessions and merge with labels, run SessionDividerHR.py.

CleanOutputFromPythonSession.m will clean NaN entries and fix the format.

If you want to take the average of sessions and reduce to one data entry for a session, use AverageSessionsHR.py

We are making use of MarcusVollmer HRVToolbox when extracting some features. Here is a link of the toolbox:
https://www.mathworks.com/matlabcentral/fileexchange/52787-marcusvollmer-hrv