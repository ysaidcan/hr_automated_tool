
interval=120
window=60
% Start with a folder and get a list of all subfolders.
% Finds and prints names of all PNG, JPG, and TIF images in 
% that folder and all of its subfolders.
clc;    % Clear the command window.
workspace;  % Make sure the workspace panel is showing.
format longg;
format compact;

% Define a starting folder.
start_path = fullfile('C:\Users\Said\Desktop\yek_files\PhD\matlab-hr-experiments\HRT');
% Ask user to confirm or change.
topLevelFolder = uigetdir(start_path);
if topLevelFolder == 0
	return;
end
% Get list of all subfolders.
allSubFolders = genpath(topLevelFolder);
% Parse into a cell array.
remain = allSubFolders;
listOfFolderNames = {};
while true
	[singleSubFolder, remain] = strtok(remain, ';');
	if isempty(singleSubFolder)
		break;
	end
	listOfFolderNames = [listOfFolderNames singleSubFolder];
end
numberOfFolders = length(listOfFolderNames)
featureMatrix=[]
% Process all image files in those folders.
for k = 1 : numberOfFolders
	% Get this folder and print it out.
	thisFolder = listOfFolderNames{k};
	fprintf('Processing folder %s\n', thisFolder);
	
	% Get PNG files.
	filePattern = sprintf('%s/*EDA.csv', thisFolder);
	baseFileNames = dir(filePattern);  
	% Add on TIF files.
	filePattern1 = sprintf('%s/*HR.csv', thisFolder);
	baseFileNames = [baseFileNames; dir(filePattern1)];
	% Add on JPG files.
	filePattern2 = sprintf('%s/*IBI.csv', thisFolder);
	baseFileNames = [baseFileNames; dir(filePattern2)];
    
    filePattern3 = sprintf('%s/*TEMP.csv', thisFolder);
	baseFileNames = [baseFileNames; dir(filePattern3)];
    
    filePattern4 = sprintf('%s/*ACC.csv', thisFolder);
	baseFileNames = [baseFileNames; dir(filePattern4)];
    
    filePattern5 = sprintf('%s/*BVP.csv', thisFolder);
	baseFileNames = [baseFileNames; dir(filePattern5)];
	numberOfImageFiles = length(baseFileNames);
	% Now we have a list of all files in this folder.
	
	if numberOfImageFiles >= 1
		% Go through all those image files.
        
		for f = 1 : numberOfImageFiles
			fullFileName = fullfile(thisFolder, baseFileNames(f).name);
			fprintf('     Processing image file %s\n', fullFileName);
            
            
            % Added for Russell label and timestamp parsing
            
            filename = fullfile(thisFolder, 'IBI.csv');
            C=strsplit(thisFolder,'_');
            EmpaticaID=C(3) %fix this
            IBI = csvread(filename,1,0);
            fid = fopen(filename);
            tline = fgetl(fid);
            while ischar(tline)
                disp(tline)
                break;
                tline = fgetl(fid);
            end
            fclose(fid);
            S=strsplit(tline,',');
            startLinuxTimeStamp=  str2double(S(1));
            timestampArr=IBI(:,1);
            IBIvalueArr=IBI(:,2);
            if(isempty(IBIvalueArr))
                continue;
            end
            %labelArr=IBI(:,1);
            %lengthOf=length(labelArr);
            startTimeStamp=timestampArr(1);
            endTimeStamp=timestampArr(end);
            %labelArr is puzzle difficulty and lengthOf is the length of
            %this array.
            numberOfWindows=(endTimeStamp-startTimeStamp)/window -1
            for windows=1 :numberOfWindows
                startTime=(windows-1)*window+startTimeStamp;
                endTime=startTime+interval;
                startIndex=min(find(timestampArr>= startTime));
                endIndex=max(find(timestampArr<=endTime ));
                IBIsegment=IBI(startIndex:endIndex,:);
                if(length(IBIsegment)<5)
                    continue;
                end
                [fft_pLF,fft_pHF,fft_LFHFratio,fft_VLF,fft_LF,fft_HF,lomb_lf,RMSSD,PNN50,TRI,TINN,lomb_hf,meanV,stdV,sdsd,qr]=FilterInterpolateWithPercentage(IBIsegment,0.2,10,2,window);
                startTime=startLinuxTimeStamp+startTime;
                endTime=startLinuxTimeStamp+endTime;
                segmentFeatures=[mean(fft_pLF),mean(fft_pHF),mean(fft_LFHFratio),mean(fft_VLF),mean(fft_LF),mean(fft_HF),mean(lomb_lf),mean(RMSSD),mean(PNN50),mean(TRI),mean(TINN),mean(lomb_hf),mean(meanV),mean(stdV),mean(sdsd),mean(qr), startTime, endTime, EmpaticaID];
                featureMatrix=[featureMatrix; segmentFeatures];
            end
		end
	else
		fprintf('     Folder %s has no image files in it.\n', thisFolder);
    end
    
end

variableNames=["fft_pLF","fft_pHF","fft_LFHFratio","fft_VLF","fft_LF","fft_HF","lomb_lf","RMSSD","PNN50","TRI","TINN","lomb_hf","meanV","stdV","sdsd","qr", "startTime", "endTime", "EmpaticaID"];
variableNames = cellstr(variableNames);

[I, ~] = find(cellfun(@(s) isequal(s, 'NaN'), featureMatrix));
featureMatrix(I, :) = [];

T = cell2table(featureMatrix,'VariableNames',variableNames); 
% Write the table to a CSV file
writetable(T,'HRprops120.csv')

A=csv2cell('HRprops120.csv','fromfile');
A(1,:) = [];
[I, ~] = find(cellfun(@(s) isequal(s, 'NaN'), A));
A(I, :) = [];
T = cell2table(A,'VariableNames',variableNames);
writetable(T,'HRprops120.csv')
