A=csv2cell('HRson.csv','fromfile');
A=A(:,2:end);
A(1,:) = [];
empties = cellfun('isempty',A);
A(empties) = {NaN};
A(any(cellfun(@(x) any(isnan(x)),A),2),:) = [];

variableNames=["fft_pLF" ,"fft_pHF" ,"fft_LFHFratio" ,"fft_VLF" ,"fft_LF" ,"fft_HF" ,"lomb_lf" ,"RMSSD" ,"startLabels" ,"endLabels", "PNN50" ,"TRI" ,"TINN" ,"lomb_hf", "meanV" ,"stdV" ,"sdsd" ,"qr" ,"SessionID" ,"SessionLabel", "EmpaticaID"];
variableNames = cellstr(variableNames);


T = cell2table(A,'VariableNames',variableNames);

T.SessionLabel(strcmp(T.SessionLabel,"b'0'")) = {0};
T.SessionLabel(strcmp(T.SessionLabel,"b'1'")) = {1};
T.SessionLabel(strcmp(T.SessionLabel,"b'2'")) = {2};
writetable(T,'HRprops120.csv')

